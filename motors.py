import logging
import asyncio as aio
from apigpio import OUTPUT, INPUT, RISING_EDGE

log = logging.getLogger()


class MotorException(Exception):
    pass


class Motor:
    """Clase para controlar un motor"""
    END_OF_RACE = 1
    MIDDLE = 0
    BEGIN_OF_RACE = -1

    def __init__(self, pi, pin_A, pin_B=None,
                 begin_of_race=None, end_of_race=None):
        self.pi = pi
        self.pin_A = pin_A
        self.pin_B = pin_B
        self.begin_of_race = begin_of_race
        self.end_of_race = end_of_race

    async def setup(self):
        "Set all the pin modes and ensure the motor is not running"
        coros = [
            *[self.pi.set_mode(pin, OUTPUT)
              for pin in (self.pin_A, self.pin_B)
              if pin is not None],
            *[self.pi.set_mode(pin, INPUT)
              for pin in (self.begin_of_race, self.end_of_race)
              if pin is not None],
        ]
        # Wait for pins setup
        await aio.wait(coros)
        # Stop the motor
        await self._set(0)

    async def where_am_i(self):
        """Returns the current motor position"""
        if self.begin_of_race is None or self.end_of_race is None:
            err = NotImplemented('Must indicate both begin of race and end of '
                                 'race pins to be able to call where_am_i')
            log.error('Error: {}'.format(err))
            raise err

        begin_of_race, end_of_race = aio.wait([
            self.pi.read(self.begin_of_race),
            self.pi.read(self.end_of_race),
        ])
        # Raise exception if both buttons are pressed
        if begin_of_race and end_of_race:
            err = MotorException(
                'Both the begin of race (GPIO {}) and the'
                'end of race buttons (GPIO {}) are pressed'.format(
                    self.begin_of_race, self.end_of_race))
            log.error('Error: {}'.format(err))
            raise err
        elif begin_of_race:
            return self.BEGIN_OF_RACE
        elif end_of_race:
            return self.END_OF_RACE
        else:
            return self.MIDDLE

    async def _set(self, direction):
        """Set the motor movement direction

        Does not verifies if the motor is already in end of race
        or begin of race positions
        The sign of ~direction~ gives the turn direction.
        if it's 0, the motor stops."""
        pin_A = self.pin_A
        pin_B = self.pin_B
        # Write opposed values in both pins
        await self.pi.write(
            pin_A, direction and bool(direction > 0) is True)
        # Ensure the pin is defined before writing to it
        if self.pin_B is not None:
            await self.pi.write(
                pin_B, direction and bool(direction > 0) is False)

    async def run_until(self, position):
        """Move the motor until end of race or begin of race
        position"""
        # We still should be able to call this function if one of
        # the buttons is indicated, but we don't need this
        if self.begin_of_race is None or self.end_of_race is None:
            err = NotImplemented('Must indicate both begin of race and end of '
                                 'race pins to be able to call run_until')
            log.error('Error: {}'.format(err))
            raise err

        if position not in [self.END_OF_RACE, self.BEGIN_OF_RACE]:
            # Do nothing
            return False
        else:
            button_pin = (self.end_of_race if position == self.END_OF_RACE
                          else self.begin_of_race)
            if await self.pi.read(button_pin):
                # Do nothing if the button is pressed
                return False
            else:
                await self.pi.wait_edge(button_pin, RISING_EDGE)


class MotorDriver:
    """Clase para controlar los motores"""
    def __init__(self, pi, move_motor, move_header_motor,
                 rotate_header_motor):
        self.pi = pi
        self.move_motor = move_motor
        self.move_header_motor = move_header_motor
        self.rotate_header_motor = rotate_header_motor
        # When this future is done, the header is active
        self.header_active_fut = aio.Future()
        # This future is defined when the header_cycle runs
        self.header_cycle_fut = None

    async def setup(self):
        "Setup all the motors and run the header cycle"
        motors = [
            self.move_motor,
            self.move_header_motor,
            self.rotate_header_motor,
        ]
        await aio.wait([motor.setup() for motor in motors])
        # Begin header cycle
        aio.ensure_future(self.header_cycle())

    def activate_header(self):
        if not self.header_active_fut.done():
            self.header_active_fut.set_result(True)

    def deactivate_header(self):
        if self.header_active_fut.done():
            self.header_active_fut = aio.Future()

    async def header_cycle(self):
        if self.header_cycle_fut is not None:
            # Only one instance running
            return
        else:
            self.header_cycle_fut = aio.Future()

        while await self.header_active_fut:
            # Activate header motor
            await self.rotate_header_motor._set(True)
            await self.move_header_motor.run_until(Motor.END_OF_RACE)
            await self.move_header_motor.run_until(Motor.BEGIN_OF_RACE)
        # Deactivate rotating motor when finished
        await self.rotate_header_motor._set(False)
        # Set the future
        self.header_cycle_fut.set_result(True)

    async def run_sequence(self):
        "Run Motor sequence"
        self.activate_header()
        # Move header
        await self.move_motor.run_until(Motor.END_OF_RACE)
        await self.move_motor_run_until(Motor.BEGIN_OF_RACE)
        # Deactivate Header()
        self.deactivate_header()

    async def shutdown(self):
        await self.move_motor.run_until(Motor.BEGIN_OF_RACE)
        if self.header_cycle_fut:
            self.deactivate_header()
            self.header_active_fut.set_result(False)
            await self.header_cycle_fut
