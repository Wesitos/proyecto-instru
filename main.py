import logging
import re
import asyncio as aio
from aiosim800 import SIM800
from smspdu import SMS_DELIVER
from os import environ as env
from apigpio import Pi, OUTPUT, INPUT, RISING_EDGE
from Adafruit_BMP.BMP085 import BMP085
from motors import Motor, MotorDriver

# Debe incluir codigo de pais
AUTHORIZED_NUMBERS = [num.strip() for num in
                      env.get('AUTHORIZED_NUMBERS', '').split(',') if num]

PIGPIO_PORT = env.get('PIGPIO_PORT', '8888')

log = logging.getLogger()


class Fona(SIM800):
    def __init__(self, pi, motor_driver, ps=None, key=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pi = pi
        self.motor_driver = motor_driver
        self.ps_pin = ps
        self.key_pin = key

    async def shutdown(self):
        await self.disconnect()
        await self.motor_driver.shutdown()

    async def power_up(self):
        if self.ps_pin is None or self.key_pin is None:
            err = Exception('ps and key pins must be provided to '
                            'be able to power up the board')
            log.error('Error: {}'.format(err))
            raise err

        if await self.pi.read(self.ps_pin):
            # Already waked up
            return True

        log.debug('Sending pulse to key pin')
        await self.pi.write(self.key_pin, True)
        await aio.sleep(0.1)
        log.debug('Pulsing LOW to key pin')
        await self.pi.write(self.key_pin, False)
        try:
            await aio.wait_for(
                self.pi.wait_edge(self.ps_pin, RISING_EDGE), 10)
        except aio.TimeoutError:
            # Cannot power up
            return False
        return True

    async def setup(self):
        ps, key = self.ps_pin, self.key_pin
        if ps is not None:
            await self.pi.set_mode(ps, INPUT)
        if key is not None:
            await self.pi.set_mode(key, OUTPUT)

    # Sensors
    async def connect_sensors(self):
        loop = aio.get_event_loop()
        self.bmp180 = await loop.run_in_executor(None, BMP085)

    async def read_sensors(self):
        loop = aio.get_event_loop()
        pressure = await loop.run_in_executor(None, self.bmp180.read_pressure)
        print('Pressure: {}pa'.format(pressure))
        return dict(pressure=pressure)

    async def handle_unsolicited(self, event):
        log.debug('Handle unsolicited')
        if event[0] != b'+CMTI':
            log.debug('Unhandled unsolicited response: {}'.format(event))
            return

        log.debug('Reading SMS: index={}'.format(event[2]))
        pdu = await self.read_sms(event[2])
        # Obviamos el mssc
        pdu = pdu[(int(pdu[:2], 16)+1)*2:].decode()
        p_deliver = SMS_DELIVER.fromPDU(pdu, 'unknown')
        remitente = p_deliver.sender
        print('Remitente:', p_deliver.sender)
        if remitente in AUTHORIZED_NUMBERS:
            print('Authorized SMS: number="{}"'.format(remitente))
            cmd = self.parse_command(p_deliver.user_data)
            if cmd:
                aio.ensure_future(self.execute_command(remitente, cmd))
        else:
            log.info('Mensaje no autorizado: number={}'.format(remitente))

    def parse_command(self, msg):
        match = re.match('(?P<cmd>[A-Z]+)(: (?P<args>[,a-zA-Z0-9]+))?', msg)
        if match:
            groups = match.groupdict()
            groups['args'] = [arg.strip() for arg in
                              groups.get('args', '').split(',') if arg]
            return groups
        else:
            return None

    # Commands
    async def execute_command(self, number, command):
        args = command['args']
        # Ensure the command exists
        handler_name = 'cmd_{}'.format(command['cmd'].lower())
        if hasattr(self, handler_name):
            print('CMD {}: number={} args={}'.format(
                command['cmd'].upper(), number, args))
            try:
                maybe_coro = getattr(self, handler_name)(number, *args)
            except TypeError:
                log.exception(
                    'TypeError when executing command: '
                    'number={} command={}'.format(number, command))
                return
            if aio.iscoroutine(maybe_coro):
                await maybe_coro

    async def cmd_hi(self, number):
        await self.send_sms(number, 'Hello')

    def cmd_log(self, number, message):
        # Do nothing
        pass

    async def cmd_run(self, number):
        "Runs the motor sequence"
        await self.motor_driver.run_sequence()

    async def cmd_sensors(self, number):
        "Responses with the sensors value"
        await self.send_sms(number, 'SENSORS:\nP: {pressure}',
                            **(await self.read_sensors()))


async def main():
    print('Iniciando')

    # Apigpio connection
    print('Connectando apigpio...', end='', flush=True)
    pi = Pi()
    await pi.connect(('127.0.0.1', PIGPIO_PORT))
    print(' connectado')

    # MotorDriver setup
    print('Inicializando motores... ', end='', flush=True)
    motor_driver = MotorDriver(
        pi,
        # Move motor
        Motor(pi, 16, 20, 21, 26),
        # Move header motor
        Motor(pi, 6, 12, 13, 19),
        # Rotate header motor
        Motor(pi, 5)
    )
    await motor_driver.setup()
    print(' hecho')

    print('Numeros autorizados: {}'.format(AUTHORIZED_NUMBERS))
    sim = Fona(pi, motor_driver, port='/dev/serial0', ps=18, key=17)
    # Sensors setup
    print('Connectando sensores...', end='', flush=True)
    await sim.connect_sensors()

    # Sim800 setup
    print(' connectado!')
    print('Encendiendo Fona...', end='', flush=True)
    await sim.setup()
    if not (await sim.power_up()):
        print('Error!')
        print('Terminando')
        await motor_driver.shutdown()
        aio.get_event_loop().stop()
        return
    print(' hecho')
    print('Connectando a SIM800... ', end='', flush=True)
    await sim.connect()
    print(' conectado!')
    # Send Notification to ourselves
    if len(AUTHORIZED_NUMBERS):
        print('Enviando')
        await sim.send_sms(AUTHORIZED_NUMBERS[0], 'STATUS: Ready')


if __name__ == '__main__':
    import sys
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    loop = aio.get_event_loop()
    aio.ensure_future(main())
    loop.run_forever()
