from asyncio import (
    Queue,
    sleep,
    ensure_future,
    IncompleteReadError,
    iscoroutine,
)
from serial_asyncio import open_serial_connection
import re
import logging
# kfyatek/smspdu

log = logging.getLogger('aioSim800')


class LimitOverrunError(Exception):
    """Reached the buffer limit while looking for a separator.
    Attributes:
    - consumed: total number of to be consumed bytes.
    """
    def __init__(self, message, consumed):
        super().__init__(message)
        self.consumed = consumed


async def readuntil(stream, separator):
    """Reads from a readableStream until a separator is found

    Copied from the cpython source for compatibility with python 3.5.1
    """
    seplen = len(separator)
    if seplen == 0:
        raise ValueError('Separator should be at least one-byte string')

    if stream._exception is not None:
        raise stream._exception

    offset = 0

    while True:
        buflen = len(stream._buffer)

        # Check if we now have enough data in the buffer for `separator` to
        # fit.
        if buflen - offset >= seplen:
            isep = stream._buffer.find(separator, offset)

            if isep != -1:
                # `separator` is in the buffer. `isep` will be used later
                # to retrieve the data.
                break

            # see upper comment for explanation.
            offset = buflen + 1 - seplen
            if offset > stream._limit:
                raise LimitOverrunError(
                    'Separator is not found, and chunk exceed the limit',
                    offset)

        if stream._eof:
            chunk = bytes(stream._buffer)
            stream._buffer.clear()
            raise IncompleteReadError(chunk, None)

        # _wait_for_data() will resume reading if stream was paused.
        await stream._wait_for_data('readuntil')

    if isep > stream._limit:
        raise LimitOverrunError(
            'Separator is found, but chunk is longer than limit', isep)

    chunk = stream._buffer[:isep + seplen]
    del stream._buffer[:isep + seplen]
    stream._maybe_resume_transport()
    return bytes(chunk)


class SIM800():
    connected = False

    def __init__(self, port):
        self.port = port
        self.read_queue = Queue()
        self.unsolicited_queue = Queue()
        self._response_loop_task = None

    async def connect(self):
        log.debug('Connecting')
        reader, writer = await open_serial_connection(url=self.port)
        self._reader = reader
        self._writer = writer
        self.connected = True
        # Runs the response loop
        self.run_response_loop()
        # Runs the unsolicited loop
        self.run_unsolicited_loop()
        # Send `escape` and \r just in case
        self._writer.write(b'\x1b\r')
        # Disable echo mode
        log.debug('Disable echo')
        await self.send_cmd('E', [0])
        await self.get_full_response()
        log.debug('Echo disabled')

    def run_unsolicited_loop(self):
        log.debug('Run unsolicited loop')
        self._unsolicited_loop_task = ensure_future(
            self.unsolicited_loop()
        )

    def run_response_loop(self):
        """Runs the response loop"""
        log.debug('Run Response loop')
        self._response_loop_task = ensure_future(
            self.response_loop())

    async def stop_response_loop(self):
        """Stops the response loop"""
        task = self._response_loop_task
        if task:
            # TODO: Be sure that unsolicited responses are not dropped
            task.cancel()
            while(not task.cancelled()):
                await sleep(0.1)

    async def unsolicited_loop(self):
        while (self.connected):
            response = await self.unsolicited_queue.get()
            log.debug('Processing unsolicited response')
            run = self.handle_unsolicited(response)
            if iscoroutine(run):
                await run

    async def response_loop(self):
        while (self.connected):
            await self._read_response()

    async def disconnect(self):
        if self.connected:
            # Write `escape` and `\r` just to be sure
            self._write.write(b'\x1b\r')
            await self._writer.drain()
            self._writer.close()
            self._writer = None
            self._reader = None
            self.connected = False

    async def _read_response(self):
        """Coroutine responsible of reading all the SIM800 responses"""
        # It's possible that there's part of a previous message in the
        # read buffer
        log.debug('Waiting for response beginning')
        before_message = await readuntil(self._reader, b'\r\n')
        if before_message != b'\r\n':
            log.debug('Se encontro un mensaje no encerrado por \\r\\n')
            response = before_message[:-2]
        else:
            # Leemos el mensaje completo
            log.debug('Waiting for response')
            partial_response = (await self._reader.read(2))  # Magic number
            # Match the +CMGS response
            if re.match(b'>', partial_response):
                response = partial_response
            else:
                # Wait for the \r\n
                log.debug('Partial response: {}'.format(partial_response))
                log.debug('Waiting for full response')
                response = partial_response + (
                    await readuntil(self._reader, b'\r\n'))[:-2]
        log.debug('Got response: {}'.format(repr(response)))
        # Lo agrega a la cola de mensajes
        parsed_response = self.parse_response(response)
        log.debug('Parsed response: {}'.format(parsed_response))
        if self.is_unsolicited(parsed_response):
            await self.unsolicited_queue.put(parsed_response)
            log.debug('Put unsolicited: {}'.format(repr(parsed_response)))
        else:
            await self.read_queue.put(parsed_response)
            log.debug('Put response: {}'.format(repr(parsed_response)))

    def parse_response(self, response):
        assert isinstance(response, bytes), '{} must be of type bytes'.format(
            repr(response))
        if response in [b'OK', b'ERROR']:
            return [response]
        if response.find(b':') == -1:
            # Response is continuation of previous response, ie: the message
            # part of a +CMGR command
            return [None, response]
        cmd, *args = response.split(b':', 1)
        if not args:
            return [cmd]
        else:
            return [cmd] + self.parse_args(args[0])

    def parse_args(self, response_args):
        # Strip the args string
        args = response_args.strip()
        if args[:1] == b'"':
            # First arg is a string
            arg_re_match = re.match(b'"(.*?)"', args)  # `+?` means non-gready
            assert arg_re_match, 'We expected a closing " in {}'.format(args)
            arg = arg_re_match.groups()[0]
            rest_args = args[arg_re_match.end():].strip().split(b',', 1)[1:]
        # Pdu
        elif not len(args) % 2  and re.match(b'[0-9A-F]+', args):
            arg = args
            rest_args = []
        # Tiene que ser un numero, sospecho que los negativos se envian
        # como cadenas, pero no estoy seguro
        elif args[:1].isdigit() or args[:1] == b'-':
            arg, *rest_args = args.split(b',', 1)
            arg = int(arg)
        else:
            raise Exception('Argumentos invalidos: {}'.format(repr(args)))
        # Unpack rest_args from list to string
        rest_args = rest_args[0] if rest_args else b''
        if rest_args:
            return [arg] + self.parse_args(rest_args)
        return [arg]

    def is_unsolicited(self, parsed_response):
        """Verifies if a received message is an unsolicited response"""
        # There are a LOT of these (SIM800 Series_AT Command
        # Manual_V1.09, pp 350 -354)
        unsolicited_set = {
            b'RING',     # Incomming call
            b'+CMTI',    # SMS received
            b'+CIEV',    # Registered in network
            b'*PSUTTZ',  # Network time refresh
            b'+CTZV',
            b'DST',
        }
        return parsed_response[0] in unsolicited_set

    async def send_cmd(self, *args, end=b'\r'):
        """Sends a command to the SIM800

        Supports sending multiple commands in the same line as long as it
        does not fill the SIM800s read buffer

        Accepts str and bytes as arguments for command names and lists or
        tuples as arguments.

        Ie. sim.send_cmd('+CMGR', [1])
        """
        assert len(args), 'Debe indicar un comando que enviar'
        # Ensure the read buffer and queue are empty
        # This means that we should process the previous command
        # response before sending a new one
        await self.clear_read_buffer_and_queue()
        cmd_stack = []
        for i, cmd in enumerate(args):
            # Verify if list or tuple (argument list)
            if isinstance(cmd, (list, tuple)):
                # Agregamos los parametros al comando anterior
                cmd_stack[-1].extend([
                    arg.encode()
                    if isinstance(arg, str) else arg
                    for arg in cmd
                ])
                continue
            # Encode if type is str (unicode)
            if isinstance(cmd, str):
                cmd = cmd.encode()
            # Strip AT prefix if present
            at_match = re.match(b'AT', cmd, re.IGNORECASE)
            if at_match:
                cmd = cmd[at_match.end():]
            cmd_stack.append([cmd])

        # Convert cmd_stack to command string
        cmd_msg_list = []
        for cmd, *args in cmd_stack:
            args = [
                str(arg).encode() if isinstance(arg, int) else
                b'"'.join([b'', arg, b''])
                for arg in args
            ]
            # Extended sintax
            if cmd[:1] == b'+':
                if len(args) is 0:
                    cmd_msg_list.append(cmd)
                else:
                    cmd_msg_list.append(b'='.join([cmd, b','.join(args)]))
            # S Parameter
            elif cmd[:1] == b'S':
                assert len(args) < 2,\
                    'Los comandos de parametro S solo reciben un parametro'
                cmd_msg_list.append(b'='.join([cmd] + args))
            # Basic sintax
            else:
                assert len(args) < 2,\
                    'Los comandos de sintaxis basica solo reciben un parametro'
                cmd_msg_list.append(b''.join([cmd] + args))

        first_cmd, *rest_cmd = cmd_msg_list
        command = b''.join(
            [b'AT', first_cmd] + [
                # We need to wrap the extended sintax commands
                b';'.join([b'', cmd, b'']) if cmd[:1] == b'+' else cmd
                for cmd in rest_cmd
            ] + [end]
        )
        log.debug('SEND_CMD: {}'.format(command))
        return self._writer.write(command)

    async def get_full_response(self):
        """Returns concatenated responses until `OK` or `ERROR`

        Returns a tuple where the first element is the command status
        (b`OK` or b`ERROR`) and the second element is the response list
        """
        log.debug('Run Get full response')
        response_list = []
        while True:
            response = await self.get_one_response()
            if response[0] in (b'OK', b'ERROR'):
                status = response[0]
                break
            response_list.append(response)
        log.debug('Got full response: {} {}'.format(status, response_list))
        return status, response_list

    async def get_one_response(self):
        """Returns one response line"""
        return await self.read_queue.get()

    async def clear_read_buffer_and_queue(self):
        """Empties the read buffer and response queue"""
        await self.stop_response_loop()
        self.read_queue = Queue()
        # Undocumented API, this could possibly break in the future
        self._reader._buffer.clear()
        self.run_response_loop()
    # SMS

    async def set_sms_format(self, format):
        """Sets the SMS message format

        format can be:
        "PDU or 0 for pdu mode
        "TEXT" or 1 for text mode
        """
        assert isinstance(format, (int, str)),\
            'SMS format must be of str or int'
        format_list = ['PDU', 'TEXT']
        if isinstance(format, str):
            format = format.upper()
            assert format in format_list, 'SMS format must be "TEXT" or "PDU"'
            format = format_list.index(format)
        await self.send_cmd('+CMGF', [format])
        status, response = await self.get_full_response()
        log.debug('SMS_FORMAT={}: {}'.format(
                repr(format_list[format]), repr(status)))
        if status == b'OK':
            return True
        else:
            return False

    async def send_sms(self, number, message):
        if isinstance(number, int):
            number = str(number).encode()
        if isinstance(message, str):
            message = message.encode()
        # Ensure text mode
        if not (await self.set_sms_format('TEXT')):
            return False

        await self.send_cmd('+CMGS', [number])
        cmgs_response = await self.get_one_response()
        # The response is a '> '
        assert cmgs_response[1] == b'>', 'Invalid +CMGS response: {}'.format(
            repr(cmgs_response))
        self._writer.write(message + b'\x1a')
        status, response = await self.get_full_response()
        if status == b'OK':
            assert len(response) == 1,\
                'Response must have length of 1 not {}'.format(len(response))
            return response[0][1]  # Return message id
        else:
            return None

    async def read_sms(self, index):
        if isinstance(index, (bytes, str)):
            index = int(index)
        await self.send_cmd('+CMGF', [0], '+CMGR', [index])
        status, response = await self.get_full_response()
        if status == b'OK':
            assert len(response) == 2, \
                'Response must have length of 2 not {}'.format(len(response))
            log.debug('Got SMS PDU: {}'.format(response[1][1]))
            return response[1][1]
        else:
            return None

    # User methods
    async def handle_unsolicited(self, event):
        pass


if __name__ == '__main__':
    from asyncio import get_event_loop

    serial_port = '/dev/ttyUSB0'

    async def main():
        sim = SIM800(serial_port)
        await sim.connect()
        print('SIM800 listo')

    loop = get_event_loop()
    ensure_future(main())
    loop.run_forever()
